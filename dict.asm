global find_word
%include "lib.inc"

section .text

find_word: 
    .loop:
        push rdi
        push rsi
        add rsi, 8
        call string_equals
        pop rsi
        pop rdi
        cmp rax, 1
        je .find
        mov rsi, [rsi]
        test rsi, rsi
        jnz .loop
    
    xor rax, rax
    ret
    .find:
        mov rax, rsi
        ret
