%include "words.inc"
%include "lib.inc"
%include "dict.inc"

%define BUFFER_SIZE 256

section .rodata
error_Elements: db "Key not found", 0
error_BuffOverflow: db "Buffer overflow"

section .bss
buffer: resb BUFFER_SIZE

section .text

global _start

_start:
    xor rdi, rdi
	mov rdi, buffer
	mov rsi, BUFFER_SIZE
	call read_word
	cmp rax, 0
	je .overflow_error
	mov rdi, buffer
	mov rsi, NEXT
	push rsi
	call find_word
	pop rsi
	cmp rax, 0
	je .notFound_error
	add rax, 8
	mov rdi, rax
	push rdi
	call string_length
	pop rdi
	add rdi, rax
	inc rdi
	call print_string
	call print_newline
	mov rdi, 0       ;success
	call exit	

.overflow_error:
	mov rdi, error_BuffOverflow
	call .printError

.notFound_error:
	mov rdi, error_Elements
	call .printError

.printError:
	push rdi
	call string_length
	mov rdx, rax
	pop rdi
	mov rsi, rdi
	mov rax, 1
	mov rdi, 2
	syscall
	call print_newline
	mov rdi, 1    ;fail
	call exit
